package com.example.task01;

import com.example.task01.TestsImpl;
import org.joor.Reflect;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Tests {

    static ITests impl;

    @BeforeClass
    public static void before() throws URISyntaxException, IOException {
        impl = new TestsImpl();
    }

    @Test
    public void testNonEmpty() {
        impl.testNonEmpty();
    }

    @Test
    public void testIfPresent() {
        impl.testIfPresent();
    }

    @Test
    public void testEmpty() {
        impl.testEmpty();
    }

    @Test
    public void testHalfEmpty() {
        impl.testHalfEmpty();
    }

    @Test
    public void testEquals() {
        impl.testEquals();
    }

}
