package com.example.task02;

import java.io.*;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
//import java.io.IOException;
//import java.lang.ClassNotFoundException;

public class SavedList<E extends Serializable> extends AbstractList<E> {
    private final File file;
    private final List<E> list;

    public SavedList(File file) {
        this.file = file;
        this.list = loadFromFile();
    }

    @Override
    public E get(int index) {
        return list.get(index);
    }

    @Override
    public E set(int index, E element) {
        E oldValue = list.set(index, element);
        saveToFile();
        return oldValue;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void add(int index, E element) {
        list.add(index, element);
        saveToFile();
    }

    @Override
    public E remove(int index) {
        E removed = list.remove(index);
        saveToFile();
        return removed;
    }

    private List<E> loadFromFile() {
        List<E> loadedList = new ArrayList<>();
        if (file.exists()) {
            try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file))) {
                loadedList = (List<E>) inputStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return loadedList;
    }

    private void saveToFile() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            outputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}