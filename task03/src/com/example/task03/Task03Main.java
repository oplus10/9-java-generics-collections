package com.example.task03;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;


public class Task03Main {

    public static void main(String[] args) throws IOException {

        List<Set<String>> anagrams = findAnagrams(new FileInputStream("task03/resources/singular.txt"), Charset.forName("windows-1251"));
        for (Set<String> anagram : anagrams) {
            System.out.println(anagram);
        }

    }

    private static boolean validCyrillicWord(String word) {
        for (char c : word.toCharArray()) {
            if (Character.UnicodeBlock.of(c) != Character.UnicodeBlock.CYRILLIC)
                return false;
        }
        return true;
    }

    public static List<Set<String>> findAnagrams(InputStream inputStream, Charset charset) {
        if (inputStream == null || charset == null)
            throw new IllegalArgumentException();

        Map<String, Set<String>> anagramMap = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset));
            String line;

            while ((line = reader.readLine()) != null) {
                String word = line.trim().toLowerCase();
                if (word.length() >= 3 && validCyrillicWord(word)) {
                    char[] chars = word.toCharArray();
                    Arrays.sort(chars);
                    String sortedWord = new String(chars);

                    Set<String> anagrams = anagramMap.getOrDefault(sortedWord, new TreeSet<>());
                    anagrams.add(word);
                    anagramMap.put(sortedWord, anagrams);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Error reading from the input stream.", e);
        }


        List<Set<String>> result = new ArrayList<>();//(anagramMap.values());
        for (Map.Entry<String, Set<String>> entry : anagramMap.entrySet()) {
            Set<String> anagrams = entry.getValue();
            if (anagrams.size() >= 2) {
                result.add(anagrams);
            }
        }

        result.sort(Comparator.comparing(set -> set.iterator().next()));

        return result;
    }
}
